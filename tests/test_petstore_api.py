import pytest
import allure
import requests


# Testing PET Entity
@allure.title("Add pet to Petstore")
@allure.story('Тестирование добавления питомца в магазин')
@pytest.mark.parametrize("pet_name, pet_category, pet_tag", [("doggie", "category_name", "tag_name")])
def test_add_pet(pet_name, pet_category, pet_tag, petstore_api):
    url = "https://petstore.swagger.io/v2/pet"

    headers = {"Content-Type": "application/json"}

    payload = {
        "id": 1,
        "category": {
            "id": 1,
            "name": pet_category
        },
        "name": pet_name,
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 0,
                "name": pet_tag
            }
        ],
        "status": "available"
    }

    response = requests.post(url, json=payload, headers=headers)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка имени"):
        assert response.json()["name"] == pet_name, f"Неверное имя"

    with allure.step("Категория"):
        assert response.json()["category"]["name"] == pet_category, f"Неверная категория"

    with allure.step("Тэг"):
        assert response.json()["tags"][0]["name"] == pet_tag, f"Неверный тэг"


@allure.title("Update pet at Petstore")
@allure.story('Тестирование изменения питомца')
@pytest.mark.parametrize("pet_data", [
    ({"id": 1, "name": "cat", "status": "sold"}),
])
def test_update_pet(pet_data):
    url = "https://petstore.swagger.io/v2/pet"
    headers = {"Content-Type": "application/json"}

    # Send PUT request to update pet
    response = requests.put(url, json=pet_data, headers=headers)
    response_data = response.json()

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка ID"):
        assert response_data["id"] == pet_data["id"], f"Неверный ID"

    with allure.step("Проверка имени"):
        assert response_data["name"] == pet_data["name"], f"Неверное имя"

    with allure.step("Проверка статуса"):
        assert response_data["status"] == pet_data["status"], f"Неверный статус"


@allure.title("Find pet by Status")
@allure.story('Поиск питомца по состоянию')
@pytest.mark.parametrize("status_code", [
    pytest.param("available", id="Available"),
    pytest.param("pending", id="Pending"),
    pytest.param("sold", id="Sold")
])
def test_find_by_status(status_code, petstore_api):
    url = "https://petstore.swagger.io/v2/pet/findByStatus"

    with allure.step("Отправляем запрос на поиск питомцев по статусу"):
        response = requests.get(url, params={"status": status_code})

    with allure.step("Проверяем статус код"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверяем наличие питомцев в списке"):
        pet_list = response.json()
        assert len(pet_list) > 0

        for pet in pet_list:
            assert pet["status"] == status_code


@allure.title("Find pet by ID")
@allure.story('Поиск питомца по ID')
def test_find_pet_by_id():
    response = requests.get("https://petstore.swagger.io/v2/pet/1")
    response_data = response.json()

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка получения ID"):
        assert response_data["id"] == 1, f"Неверный ID"


@allure.title("Upload Image for Pet")
@allure.story('Загрузить изображение питомца')
def test_upload_image_for_pet():
    image_file = open('tests/test.png', "rb")
    files = {"file": image_file}

    response = requests.post("https://petstore.swagger.io/v2/pet/1/uploadImage", files=files)
    response_data = response.json()

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Загрузка изображения"):
        assert response_data["type"] == "unknown"


# Testing USER Entity
@allure.title("Create with Array")
@allure.story('Тестирование добавления пользователя в магазин')
@pytest.mark.parametrize("user_name, user_email, user_password", [("Jhonny", "Jhonny@gmail.com", "12345")])
def test_create_user_with_array(user_name, user_email, user_password):
    url = 'https://petstore.swagger.io/v2/user/createWithArray'
    headers = {'Content-Type': 'application/json'}

    data = [
        {
            "id": 1,
            "username": user_name,
            "firstName": "Jonh",
            "lastName": "Doe",
            "email": user_email,
            "password": user_password,
            "phone": "111-11-11",
            "userStatus": 0
        }
    ]

    response = requests.post(url, json=data, headers=headers)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка типа"):
        assert response.json()["type"] == "unknown", f"Неверный тип"

    assert response.json()['message'] == 'ok'


@allure.title('Get user by username')
@allure.story('Инфо о пользователе по логину')
def test_get_user_by_username():
    username = 'Jhonny'
    url = f'https://petstore.swagger.io/v2/user/{username}'
    response = requests.get(url)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка имени"):
        assert response.json()["username"] == username, f"Неверное имя"


@allure.title('Login user')
@allure.story('Вход пользователя')
def test_login_user():
    url = 'https://petstore.swagger.io/v2/user/login'
    headers = {'Content-Type': 'application/json'}
    data = {
        "username": "Jhonny",
        "password": "12345"
    }
    response = requests.get(url, json=data, headers=headers)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"


@allure.title('Update user')
@allure.story('Изменение данных пользователя')
@pytest.mark.parametrize("user_password, user_phone", [("54321", "222-22-22")])
def test_update_user(user_password, user_phone):
    username = 'Jhonny'
    url = f'https://petstore.swagger.io/v2/user/{username}'
    headers = {'Content-Type': 'application/json'}

    data = {
        "id": 1,
        "username": "Jhonny",
        "firstName": "John",
        "lastName": "Doe",
        "email": "Jhonny@gmail.com",
        "password": user_password,
        "phone": user_phone,
        "userStatus": 1
    }

    response = requests.put(url, json=data, headers=headers)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка типа"):
        assert response.json()["type"] == "unknown", f"Неверный тип"

    assert response.json()['message'] == 'ok'


@allure.title('Logout user')
@allure.story('Выход пользователя')
def test_logout_user():
    url = 'https://petstore.swagger.io/v2/user/logout'
    response = requests.get(url)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"


# Testing STORE Entity
@allure.title('Place an order for a pet')
@allure.story('Размещение заказа на питомца')
def test_place_order():
    url = "https://petstore.swagger.io/v2/store/order"
    payload = {
        "id": 1,
        "petId": 1,
        "quantity": 1,
        "shipDate": "2024-01-13T19:19:26.397Z",
        "status": "placed",
        "complete": True
    }
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.post(url, json=payload, headers=headers)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка ID"):
        assert response.json()["id"] == payload["id"], f"Неверный ID"


@allure.title('Find purchase order by ID')
@allure.story('Поиск заказа на питомца по ID')
def test_find_order_by_id():
    order_id = 1
    url = f"https://petstore.swagger.io/v2/store/order/{order_id}"
    response = requests.get(url)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Проверка ID"):
        assert response.json()["id"] == order_id, f"Неверный ID"


@allure.title('Delete purchase order by ID')
@allure.story('Удалить заказ на питомца по ID')
def test_delete_order_by_id():
    order_id = 1
    url = f"https://petstore.swagger.io/v2/store/order/{order_id}"
    response = requests.delete(url)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"


@allure.title('Returns pet inventories by status')
@allure.story('Удалить заказ на питомца по ID')
def test_get_inventory():
    url = "https://petstore.swagger.io/v2/store/inventory"
    response = requests.get(url)

    with allure.step("Код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"

    with allure.step("Продано"):
        assert response.json()["sold"] == 13, f"Неверное значение"

    with allure.step("Дома"):
        assert response.json()["at home"] == 1, f"Неверное значение"

    assert "available" in response.json()
